var request = require('supertest'); 
var app = require('../server.js')   
describe('GET /', function () { 
    it('displays "Hello world heree!"', function (done) {
        request(app).get('/').expect('Hello world heree!', done);
    }); 
});  
